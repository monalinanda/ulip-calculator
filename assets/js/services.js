let getComponent = (component, props) => {
    return import(component).then(function(module) {
        return module.default(props);
    });
};


export default {
        getComponent: getComponent
}