let cardjs = "./widget/card/card.js";
let genericInputjs = "./widget/input/normal-input.js";
let genericRadioCardjs = "./widget/input/radio-card/generic-radio-wrapper.js";
let sliderjs = "./widget/input/slider/slider.js";
let buttonjs = "./widget/button/button.js";

let step = 1;

let cardProps = {
  appendAtId: "card-wrapper"
};
let NameProps = {
  inputId: "name",
  label: "Name:",
  type: "text",
  appendAtId: "card-inputblock-0",
  errorText: "Enter valid name."
};
let EmailProps = {
  inputId: "email",
  label: "Email Id:",
  type: "email",
  appendAtId: "card-inputblock-4",
  errorText: "Enter valid email."
};
let MobileNumberProps = {
  inputId: "phone",
  label: "Mobile Number:",
  type: "numbers",
  appendAtId: "card-inputblock-3",
  inputPrefix: "+91",
  errorText: "Enter valid phone number."
};

let GenderInputProps = {
  label: "Gender:",
  name: "gender",
  type: "radio",
  appendAtId: "card-inputblock-2",
  inputId: "genderValue",
  options: {
    Male: {
      checked: true,
      image: {
        classList: ["male-input-image", "gender-input-image"]
      }
    },
    Female: {
      image: {
        classList: ["female-input-image", "gender-input-image"]
      }
    }
  }
};

let AgeSliderProps = {
  label: "Age :",
  appendAtId: "card-inputblock-1",
  minValue: 20,
  maxValue: 70,
  defaultValue: 34,
  suffixValue: "years old",
  inputId: "age"
};

let previousButtonProps = {
  appendAtId: "card-inputblock-5",
  buttonId: "car1-previousBtn",
  buttonText: "Previous",
  buttonPrefixClass: ["previousbtn"],
  buttonClass: "previousbtn",
  buttonType: "next"
};

let nextButtonProps = {
  appendAtId: "card-inputblock-6",
  buttonId: "car1-previousBtn",
  buttonText: "Next",
  buttonPrefixClass: ["nextbtn"],
  buttonClass: "nextbtn",
  validate: validateForm
};

// fetching element where calculator widget need to be attached
let calculatorblock = document.getElementById("#calculator");

let cardWrapper = document.createElement("div");
cardWrapper.setAttribute("id", "card-wrapper");
calculatorblock.appendChild(cardWrapper);

function addStyleSheet() {
  let styleSheet = "./index.css";
  let link = document.createElement("link");
  let head = document.getElementsByTagName("head");
  head[0].append(link);
  props = {
    href: styleSheet,
    rel: "stylesheet",
    type: "text/css"
  };
  setAttr(link, props);
}

function setAttr(el, props) {
  Object.keys(props).map(key => {
    el.setAttribute(key, props[key]);
  });
}

let getComponent = (component, props) => {
  return import(component).then(function(module) {
    return module.default(props);
  });
};
/*
let Card = getComponent(execute, cardjs, cardProps);
let NameInput = getComponent(execute, genericInputjs, NameProps);
let EmailInput = getComponent(execute, genericInputjs, EmailProps);
let MobileNumberInput = getComponent(
  execute,
  genericInputjs,
  MobileNumberProps
);
let GenderInput = getComponent(execute, genericRadioCardjs, GenderInputProps);
let AgeSlider = getComponent(execute, sliderjs, AgeSliderProps);
let previousBtn = getComponent(execute, buttonjs, previousButtonProps);
let nextBtn = getComponent(execute, buttonjs, nextButtonProps);
*/

function validateForm(
  fields = {
    name: document.getElementById("name"),
    email: document.getElementById("email"),
    phone: document.getElementById("phone"),
    gender: document.getElementById("genderValue"),
    age: document.getElementById("age")
  }
) {
  if (
    fields.name.value &&
    fields.email.value &&
    fields.phone.value &&
    fields.gender.innerText &&
    fields.age.value
  ) {
    console.log("You are redirected to next page");
  }
}

if (step == 1) {
  card1 = {
    card: getComponent(cardjs, cardProps),
    NameInput: getComponent(genericInputjs, NameProps),
    EmailInput: getComponent(genericInputjs, EmailProps),
    MobileInput: getComponent(genericInputjs, MobileNumberProps),
    GenderInput: getComponent(genericRadioCardjs, GenderInputProps),
    AgeSlider: getComponent(sliderjs, AgeSliderProps),
    previousBtn: getComponent(buttonjs, previousButtonProps),
    nextBtn: getComponent(buttonjs, nextButtonProps)
  };
} else if (step == 2) {
}

function init() {
  addStyleSheet();
}

init();
