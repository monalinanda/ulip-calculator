export default props => {
  let button = document.createElement("button");
  button.setAttribute("id", props.buttonId);
  button.setAttribute("type", "button");
  button.classList.add(props.buttonClass);
  document.getElementById(props.appendAtId).appendChild(button);

  if (props.buttonPrefixClass) {
    let buttonPrefixImage = document.createElement("div");
    if (props.buttonPrefixClass) {
      for (let i = 0; i <= props.buttonPrefixClass; i++) {
        button.classList.add(props.buttonPrefixClass[i]);
      }
    }
    button.appendChild(buttonPrefixImage);
  }

  let buttonText = document.createElement("span");
  button.appendChild(buttonText);
  buttonText.innerText = props.buttonText;

  if (props.buttonSuffixClass) {
    let buttonSuffixImage = document.createElement("div");
    if (props.buttonPrefixClass) {
      for (let i = 0; i <= props.buttonPrefixClass; i++) {
        button.classList.add(props.buttonPrefixClass[i]);
      }
    }
    button.appendChild(buttonSuffixImage);
  }

  button.addEventListener("click", () => {
    console.log("clicked");
    props.validate();
  });
};
