//exporting default card components

export default props => {
  let card = document.createElement("div");
  card.setAttribute("id", "card");
  card.setAttribute("class", "Rectangle");
  for(let i=0;i<7;i++){
    let inputBlock = document.createElement('div')
    inputBlock.setAttribute('id',`card-inputblock-${i}`)
    card.appendChild(inputBlock)
  }
  // return card
  document.getElementById(props.appendAtId).appendChild(card);
};