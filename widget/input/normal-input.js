// exporiting default normal-input components

export default props => {
  props.label ? "" : (props.label = "default");
  props.type ? "" : (props.type = "text");
  props.regex ? "" : (props.regex = new RegExp("^([- a-zA-Z\u00c0-\u024f]+)$"));

  // wrapper div
  let inputwrapper = document.createElement("div");
  inputwrapper.setAttribute("class", "Input-Wrapper");

  // input element
  let normalInput = document.createElement("input");
  normalInput.setAttribute("id", props.inputId);
  normalInput.setAttribute("type", props.type);
  normalInput.setAttribute("class", "Input");

  // input's label
  let normalLabel = document.createElement("label");
  normalLabel.setAttribute("class", "Name");
  normalLabel.innerText = props.label;

  //input prefix span
  if (props.inputPrefix) {
    let inputPrefix = document.createElement("span");
    inputPrefix.innerText = props.inputPrefix;
    normalLabel.append(inputPrefix);
  }

  normalLabel.append(normalInput);
  inputwrapper.append(normalLabel);

  let errorDiv = document.createElement("div");
  inputwrapper.appendChild(errorDiv);

  // return inputwrapper
  document.getElementById(props.appendAtId).appendChild(inputwrapper);

  normalInput.addEventListener("keyup", e => {
    let regex = new RegExp(props.regex);
    let val = e.currentTarget.value;
    let errorText;
    regex.test(val) ? (errorText = "") : (errorText = props.errorText);
    errorDiv.innerText = errorText;
  });
};
