//  Default slider component

export default props => {
  let sliderWrapper = document.createElement("div");
  sliderWrapper.classList.add("sliderWrapper");
  document.getElementById(props.appendAtId).appendChild(sliderWrapper);

  // label for input
  let label = document.createElement("label");
  label.innerText = props.label;
  sliderWrapper.appendChild(label);
  // label for input

  // slider input starts here
  let slider = document.createElement("div");
  slider.classList.add("rangeInputWrapper");
  sliderWrapper.appendChild(slider);

  let rangeInput = document.createElement("input");
  rangeInput.setAttribute("type", "range");
  rangeInput.setAttribute("min", props.minValue);
  rangeInput.setAttribute("max", props.maxValue);
  rangeInput.classList.add("slider");
  rangeInput.value = props.defaultValue;
  rangeInput.setAttribute("id", props.inputId);
  rangeInput.classList.add("rangeInput");
  slider.appendChild(rangeInput);

  let guideDiv = document.createElement("div");
  guideDiv.classList.add("guideDiv");
  slider.appendChild(guideDiv);
  for (let scale = props.minValue; scale <= props.maxValue; scale = scale + 5) {
    let scaleValue = document.createElement("span");
    scaleValue.innerText = scale;
    guideDiv.appendChild(scaleValue);
  }

  // slider input ends here

  // inputValueWrapper starts here with all its sub component
  let inputValueWrapper = document.createElement("div");
  inputValueWrapper.classList.add("inputValueWrapper");
  sliderWrapper.appendChild(inputValueWrapper);

  if (props.prefixValue) {
    let prefix = document.createElement("div");
    prefix.classList.add("rangeInputPrefixSuffix")
    prefix.innerText = props.prefixValue;
    inputValueWrapper.appendChild(prefix);
  }

  let inputValue = document.createElement("div");
  inputValue.innerText = props.defaultValue;
  inputValue.setAttribute("id", "rangeInputValue");
  inputValue.classList.add("rangeInputValue");
  inputValueWrapper.appendChild(inputValue);

  if (props.suffixValue) {
    let suffix = document.createElement("div");
    suffix.classList.add("rangeInputPrefixSuffix")
    suffix.innerText = props.suffixValue;
    inputValueWrapper.appendChild(suffix);
  }
  // inputValueWrapper ends here with all its sub component within it

  //functionality to handle color change
  // document.getElementById(props.inputId).addEventListener("input", (e) => {
  //   debugger;
  //   e.currentTarget.style.background = 'linear-gradient(to right, #f58220, #005555 ' + e.currentTarget.value + '%)';
  // });

  // functionality to handle changing input
  document.getElementById(props.inputId).addEventListener("change", e => {
    document.getElementById("rangeInputValue").innerText =
      e.currentTarget.value;
  });
};
